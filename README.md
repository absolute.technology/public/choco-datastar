# DATASTAR

DATASTAR is a unique set of development tools to enable you to version database components including data utilizing your existing source control system. With DATASTAR you describe how you want to componentize your data and then use the software to generate scripts which encapsulates this data as components. DATASTAR integrates with your source control system so that you can identify changes and package a deployment accordingly. This repo contains the Chocolatey packaging source files, for further details see [https://datastar.software](https://datastar.software)

**Notes**
This package uses the DataStar MSI installer to install DataStar via Chocolatey.


